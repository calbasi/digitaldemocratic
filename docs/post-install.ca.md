# Instruccions post-instal·lació DD

### Accesos locales login:

* MOODLE: https://moodle.DOMINI/login/index.php?saml=off

* NEXTCLOUD: https://nextcloud.DOMINI/login?direct=1

* WORDPRESS: https://wp.DOMINI/wp-login.php?normal


## -1. Instal·lació de software:

Per generar certificats multidomini y del domini principal:
```
apt install rsync vim tmux certbot -y
DOMAIN=digitaldemocratic.net
certbot certonly --preferred-challenges dns --manual --email digitaldemocratic@$DOMAIN --agree-tos -d *.$DOMAIN,$DOMAIN

```

Donar d'alta al dns el wildcard o els subdominis:
 - moodle
 - nextcloud
 - wp
 - oof
 - sso
 - pad
 - admin



## 0. Esborrar dades i/o configs

Si volem començar des de cero podem esborrar les dades i el repositori de codi (opcional)

Esborrar dades:
```
./dd-ctl reset-1714

```

Esborrar dades, configs, codi i certificats:

```
cd /opt/digitaldemocratic/src
./dd-ctl reset-1714
cd ..
rm -rf /opt/digitaldemocratic/src

hostname=test1
cp /opt/src/digitaldemocratic/digitaldemocratic.conf /opt/src/digitaldemocratic.conf.backup

git clone https://gitlab.com/digitaldemocratic/digitaldemocratic /opt/src/digitaldemocratic
cd /opt/src/digitaldemocratic
cp digitaldemocratic.conf.sample digitaldemocratic.conf
cp -r custom.sample custom
./securize_conf.sh
# Canvia els noms de domini de la configuració del dd pel hostname de la màquina
sed -i "s/DOMAIN=mydomain.com/DOMAIN=$hostname.digitaldemocratic.net/g" digitaldemocratic.conf
sed -i "s/LETSENCRYPT_DNS=/LETSENCRYPT_DNS=$hostname.digitaldemocratic.net/g" digitaldemocratic.conf
sed -i "s/LETSENCRYPT_EMAIL=/LETSENCRYPT_EMAIL=info@digitaldemocratic.net/g" digitaldemocratic.conf

./dd-ctl repo-update
```


## 1. Instal·lació des de Zero:

- Clonar el directori de digital democratic
```bash
mkdir /opt/src
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic /opt/src/digitaldemocratic
cd /opt/src/digitaldemocratic
cp digitaldemocratic.conf.sample digitaldemocratic.conf

#update del repo i subrepos
./dd-ctl repo-update

#variables aleatories de config
bash securize_conf.sh
```
- Canviem el nom del domini i si omplim les variables de LETSENCRYPT ja genera els certificats
```bash
TITLE="Digital Democratic"
TITLE_SHORT="dd"
DOMAIN=digitaldemocratic.net
LETSENCRYPT_DNS=digitaldemocratic.net
LETSENCRYPT_EMAIL=suport-baixeras@digitaldemocratic.net
# Generate letsencrypt certificate for root domain
# Values:
# - false (default): dont generate certificate for root domain, only for                                                                  
#                    subdomains.
# - true: generate certificate for root domain and subdomains.
LETSENCRYPT_DOMAIN_ROOT=true

```
- Copiem el custom.sample i fem un ./dd-ctl all
```bash
cp -r custom.sample custom
./dd-ctl all
```


## 2. Configuració:

### 2.1. Configuració del Keycloak

Go to https://sso.DOMINI/auth/admin/master/console

**THEMES**:
- [ ] login theme: liiibrelite
- [ ] account theme: account-avatar
- [ ] internazionalization enabled: ON
- [ ] default locale: ca



1. Configure -> Realm Settings -> Themes

Configurem d'aquest manera:

![](img/snapshot/1FGGqna.png)

**SECURITY DEFENSES**:
- [ ] Canviar segona línia de Content-Security-Policy per:
`frame-src 'self'; frame-ancestors *; object-src 'none';`

- [ ] La última per:
`max-age=31536000; includeSubDomains`

- [ ] Save

![](img/snapshot/uS5uqJB.png)

**CLIENT SCOPES**:

- [ ] client scopes => mappers => role_list => Single Role Attribute: ON

![](img/snapshot/Q2i349B.png)

![](img/snapshot/KYbY4ao.png)

![](img/snapshot/oJJPRdp.png)







### 2.2. Configuració Wordpress

![](img/snapshot/Nk8YPCI.png)

![](img/snapshot/3ZRPyzd.png)

Configurar el nickname de Wordpress:
![](img/snapshot/uOwYjOJ.png)

Script: 
```
var Output = user.getFirstName()+" "+user.getLastName();
Output;
```

**Per a que et permeti tancar sessió de SAML des de Wordpress:**

![](img/snapshot/myofFZv.png)

Afegim aquests paràmetres:

`/realms/master/account/*`
`https://wp.DOMAIN/*`

![](img/snapshot/7U9t8Zn.png)

Guardem la configuració.

Verificar que el plugin GenerateBlock i el tema GeneratePress están instalados y activados 

![](img/snapshot/gZGNZXY.png)

![](img/snapshot/iThTdIa.png)









### 2.3. Configuració Moodle

Fer login com a admin de Moodle a: https://moodle.DOMINI/login/index.php?saml=off

* Quan entres per primer cop al moodle et surt això:

![](img/snapshot/DqdZ53y.png)

Li donas a 'Continua' i et surt això:

![](img/snapshot/NsvzuXP.png)

![](img/snapshot/V61v4RX.png)

![](img/snapshot/qIDjX8b.png) 

*En aquesta imatge pots afegir el nom del lloc, i altres paràmetres.*

![](img/snapshot/qCzZjkv.png)

![](img/snapshot/zzvSxW7.png)



### 2.4. Configuració Nextcloud

- Per configurar el email: 

![](img/snapshot/5jIt2EE.png)
![](img/snapshot/gMQAKmb.png)


**- Cercles:**

1. Per descarregar els Cercles: Aplicacions -> Aplicacions destacades -> Circles (Descarrega i activa)

![](img/snapshot/yyNyUvc.png)

2. Ara sortirà un menú nou

![](img/snapshot/IbRuJqC.png)

3. Tornem a la pantalla de paràmetres i anem a la secció de Administració -> "Treball en grup" o "Groupware":

![](img/snapshot/yjbOrLz.png)

O bé per linies de comandes:

```
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles members_limit --value="150"
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles allow_linked_groups --value="1"
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles skip_invitation_to_closed_circles --value="1
```

### Neteja de caché del keycloak

Fer les comandes **una a una**:

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=false)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=false)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=-1)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=true)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=true)' `

` docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=2592000)'` 

` docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload' `

## 3. Moodle Post-Install (Personalització)

### 3.1. Per depurar cachés: (https://moodle.DOMINI/admin/purgecaches.php)

![](img/snapshot/lw5g14Y.png)

### 3.2. Per desactivar cron:

![](img/snapshot/xT4A2dg.png)

![](img/snapshot/9QBhx4Z.png)

![](img/snapshot/nKgcbYq.png)


### 3.3. Selecció de tema
![](img/snapshot/JiRFb8Q.png)
![](img/snapshot/fmWmDwX.png)

Configurar a gust:
![](img/snapshot/TEpFOaG.png)

O bé clicant l'enllaç: https://moodle.DOMINI/admin/settings.php?section=themesettingcbe
![](img/snapshot/mfMrRjD.png)

**A L'ACTUALITZAR ELS PLUGINS DE MOODLE SURT UN MENÚ NOU!!!**

![](img/snapshot/lZJL9DA.png)


### 3.4. Per configurar els blocs

* Afegir blocs:

![](img/snapshot/7zHXvZG.png)
![](img/snapshot/x91xIW4.png)


### OJO no liarla, que hay que ir a esta página para que aplique a todos los usuarios
https://moodle.DOMINI/my/indexsys.php 
![](img/snapshot/KHofHbp.png)
![](img/snapshot/OqTKDC6.png)

* Eliminar els blocs:
    *  Plans d'aprendizatge: DESACTIVAR
    *  Cursos visitats recentment: DESACTIVAR
    *  Fitxers privats: DESACTIVAR
    *  Usuaris en línia: DESACTIVAR
    *  Esdeveniments propers: DESACTIVAR
    *  Insígnies recents: DESACTIVAR
    *  3iP Mods pending: AFEGEIX

Ens hauria de quedar així:

![](img/snapshot/jFds8IG.png)

Y clic al botón de restablecer a todos los usuarios

![](img/snapshot/FNwazUg.png)


Altres configuracions de Moodle:

```bash 
docker exec isard-apps-moodle php7 /var/www/html/admin/cli/cfg.php --name=guestloginbutton --set=0
docker exec isard-apps-moodle php7 /var/www/html/admin/cli/cfg.php --name=enrol_plugins_enabled --set=manual
docker exec isard-apps-moodle php7 /var/www/html/admin/cli/cfg.php --name=enablemobilewebservice --set=0
docker exec isard-apps-moodle php7 /var/www/html/admin/cli/cfg.php --name=enablebadges --set=0
```


### 3.5. Permisos de rol dels usuaris

![](img/snapshot/emep2Qz.png)

Treure aquests permisos a l'usuari autenticat:
![](img/snapshot/Jv5WMey.png)



- moodle/my:manageblocks : DESACTIVAR

- moodle/user:manageownfiles: DESACTIVAR

- moodle/user:editownmessageprofile: DESACTIVAR

- moodle/user:editownprofile: ACTIVAR - (OJO! Esto se ha vuelto a ACTIVAR, por defecto está ACTIVADO, y hay que dejarlo así)

- report/usersessions:manageownsessions: DESACTIVAR

- moodle/user:manageownblocks: DESACTIVAR

Per editar els rols d'aquesta manera:
![](img/snapshot/tJjyWER.png)

Cliquem l'icona i anem a la part de baix de la pàgina per assignar els permisos. Ens trobarem amb aixó:

![](img/snapshot/H4Tpjyu.png)
A baix podem veure els permisos amb la nomenclatura esmentada. Seleccionem els checkboxes corresponents.

Guardem la configuració:

![](img/snapshot/NJE4lA3.png)



- Forçar als usuaris a identificar-se en politiques de seguretat (https://moodle.DOMINI/admin/settings.php?section=sitepolicies): ACTIVAR
![](img/snapshot/UF94Xa1.png)


### 3.6. Paràmetres de configuració

- Desactivar competències (https://moodle.DOMINI/admin/settings.php?section=competencysettings):
![](img/snapshot/9nkK4Qp.png)


- Seguiment de compleció (https://moodle.DOMINI/admin/settings.php?section=coursesettings): 
![](img/snapshot/ZUQm0wE.png)

- Private sessions Jitsi (https://moodle.DOMINI/admin/settings.php?section=modsettingjitsi):
![](img/snapshot/pNUknn3.png)



### 3.7. NextCloud Submission

Anar a:

![](img/snapshot/WUG17ft.png)

Omplir el formulari:
![](img/snapshot/CXVmKXr.png)

Host NextCloud:
https://nextcloud.DOMINI.cat

User NextCloud:
admin

Nom de la plantilla:
template.docx

(a test1 ja està)

---

Desar els canvis i purgar [la caché](https://hackmd.io/Pqia-LNAScyGfI27oB8ScQ?both#Neteja-de-cach%C3%A9). 

### 3.8 Capabilities

- Course Creator
    - Allow:
        - moodle/course:manageactivities
        - mod/bigbluebuttonbn:addinstance
        - moodle/course:delete
        - mod/tresipuntshare:addinstance
        - mod/tresipuntshare:view
        - mod/assign:viewownsubmissionsummary
        - moodle/user:manageownfiles - Esto permite al creador de curso ver sus archivos privados. Se eliminará cuando estén en NextCloud. Se utiliza para la importación de la carpeta del profesor de Google Classroom
        - repository/user:view - Esto permite al creador de curso ver el repositorio del archivos privados cuando añade un archivo a la actividad o recurso. Se eliminará cuando estén en NextCloud. Se utiliza para la importación de la carpeta del profesor de Google Classroom
![](img/snapshot/PZZuCo5.png)

- Teacher
    - Allow:
        - Que un professor pugui matricular a altres professors.
        Anem a la pàgina https://moodle.test1.digitaldemocratic.net/admin/roles/allow.php?mode=assign
        
        I anem a la pestanya "Permetre asignar rols"


![](img/snapshot/uI92ICp.png)


- Asignar el rol de 'Profesor' al Creador de Curs

### 3.9 Core

### 3.10 Plugins

- Theme CBE

    - Host (theme_cbe | host): test1.digitaldemocratic.net
    - Logo URL (theme_cbe | logourl): https://api.test1.digitaldemocratic.net/img/logo.png
    - API de cabecera y colores (theme_cbe | header_api): SÍ
    - Avatar API (theme_cbe | avatar_api): SÍ
    - URL Avatar API (theme_cbe | avatar_api_url): https://sso.test1.digitaldemocratic.net/auth/realms/master/avatar-provider
    - URL Avatar Otros Usuarios API (theme_cbe | avatar_other_users): https://api.test1.digitaldemocratic.net/avatar/
    - URL Perfil API (theme_cbe | avatar_profile_url): https://sso.test1.digitaldemocratic.net/auth/realms/master/account
    - Enlace Digital Democratic (theme_cbe | has_dd_link): SÍ
    - URL Digital Democratic (theme_cbe | ddlink_url): https://xnet-x.net/ca/digital-democratic/

    - Importar Cursos de Google Classroom (theme_cbe | importgc): SÍ
    - Direct virtual class (theme_cbe | vclasses_direct): SÍ
    - Utilizar Nombre único (theme_cbe | uniquenamecourse): SÍ
    - Internal APPs another tab (theme_cbe | apssallexternals): SÍ
    - URL de creación de archivo de NextCloud (theme_cbe | hostnccreate): https://nextcloud.test1.digitaldemocratic.net/apps/files

    - URL Privacy Policies (theme_cbe | policies): PENDIENTE
    - URL Center Legal Notice (theme_cbe | aviso_legal): PENDIENTE

### 3.11 Repositorio NextCloud

1. Dentro de NextCloud hay que crear un cliente

![](img/snapshot/3ICWP5X.png)

- Name: moodle
- URI: https://moodle.test1.digitaldemocratic.net/admin/oauth2callback.php

Se crea un **Id Cliente** y un **Secret** que hay que añadir en el oAuth2 de Moodle.


2. Crear servicio oAuth2 

https://moodle.test1.digitaldemocratic.net/admin/tool/oauth2/issuers.php

Crear nuevo servicio NextCloud

![](img/snapshot/mkM8JN1.png)

Configurar con estos datos:

- Name: NextCloud
- Client Id: **Id Cliente**
- Client Secret: **Secret**
- [OK] Autenticar solicitudes de token a través de encabezados HTTP
- URL base de servicio: https://nextcloud.test1.digitaldemocratic.net

![](img/snapshot/KBV5ys2.png)

Para probar que funciona damos al siguiente icono
![](img/snapshot/XLQNA9i.png)

Y seguimos los pasos de autenticación que nos marca NextCloud. Si aparece el Tic verde, estaría bien configurado.


3. Hay que ir a 'Manage repositories' https://moodle.test1.digitaldemocratic.net/admin/repository.php

Activar y poner visible

Ir al Settings del Repositorio NextCloud

![](img/snapshot/JGRbAJF.png)

Activar ambas opciones y salvar

![](img/snapshot/buRSMwg.png)

Crear una instancia del Repositorio

- Name: NextCloud
- Issuer: Seleccionamos el oAuth2 que hemos creado anteriormente
- Folder: ''
- Supported files: Internal y External
- Return typ: Internal

[SALVAR CAMBIOS]



### 3.12 Exportar e importar traduciones

1. Desde un entorno con las traducciones completadas hay que ir  https://moodle.demo.digitaldemocratic.net/admin/tool/customlang/index.php

2. Escoger el idioma y exportar

![](img/snapshot/ijdcfjP.png)
 
3. Ir a la plataforma donde queremos importar https://moodle.test1.digitaldemocratic.net/admin/tool/customlang/index.php
4. Seleccionamos el idioma y subir el archivo
![](img/snapshot/XrTRamv.png)

5. Importante!! hay que guardar

![](img/snapshot/bFEEg8n.png)

### 3.13 Mostrar solament activitats i recursos determinats

https://moodle.test1.digitaldemocratic.net/admin/modules.php

Activitats visibles:

- Tarea
- BigBlueButton
- Encuesta
- Carpeta
- Foro
- Glosario
- H5P
- Etiqueta
- Página
- Cuestionario
- Archivo
- Audio
- Compartir publicación
- Video
- URL

![](img/snapshot/rrR9HSg.png)

### 3.14 Activitats recomenades

https://moodle.test1.digitaldemocratic.net/course/recommendations.php

S'activen:
 - Assignment
 - File
 - Folder
 - URL

![](img/snapshot/vmGtZ4T.png)

### 3.15. Configuració BigBlueButton

Administració de lloc -> Connectors -> Mòduls d'activitat -> BigBlueButtonBN

![](img/snapshot/zRvcEEA.png)

![](img/snapshot/r8XBwX9.png)

![](img/snapshot/VNYZcPx.png)

![](img/snapshot/VwxTUA8.png)





## 4. SAML PLUGINS ACTIVATION

### 4.1. Plugin de SAML2 Moodle

**1. Login a moodle com admin via: https://moodle.\DOMINI/login/index.php?saml=off**

Username i password:
`cat /opt/src/digitaldemocratic/digitaldemocratic.conf | grep -i moodle_admin`

![](img/snapshot/zhdMsjM.png)


**2. Entrar a "Administració del lloc" -> Connectors -> Autenticació (SAML2)**

![](img/snapshot/SZ19er2.png)

![](img/snapshot/KAkxMMZ.png)

![](img/snapshot/eGDFcpi.png)


**3. Clic en el botó "Regenerate certificate", acceptar la configuració tal qual i tornar a la pàgina de configuració de SAML2**

![](img/snapshot/dKD6P2Q.png)
Fem clic a regenerate

**4. Clic en el botó "Lock certificate"**

![](img/snapshot/mupXZ7B.png)

**5. Al terminal, executar el script per autoconfigurar:**

```
docker exec isard-sso-admin python3 /admin/saml_scripts/moodle_saml.py
```

**6. Cal fer uns últims retocs:**

A la mateixa pàgina on ens trobem:

- Per a que ja no et demani entrar com a Moodle i vagi automàticament al SSO:
![](img/snapshot/tnx383P.png)


- Auto create users = Sí
![](img/snapshot/hsYy7LW.png)




### 4.2. Plugin de Wordpress SAML2


**1. Entrar com admin al WordPress (has d'estar amb la sessió tancada als altres entorns.):  https://wp.\<domain\>/wp-login.php?normal**

**2. Activar el plugin "OneLogin SAML SSO" i aplicar els canvis**





### 4.3. Configuració de WordPress

- Per configurar la hora i el idioma

![](img/snapshot/JbyHUqJ.png)






## 5.0 Otros


### 5.1 extra moodle


### 5.2 altres extra

Redirect URIs para logout de wordpress

En keycloack -> clients -> account:

![](img/snapshot/oqyl6px.png)

Anar a "Valid Redirect URIs" i afegir la url de wordpres amb https y * al final: **https://wp.test1.digitaldemocratic.net/***

![](img/snapshot/GWF38UE.png)



habrá que documentar como crear en nextcloud el pass

![](img/snapshot/cZcZUfV.png)





