#!/bin/bash
source ../digitaldemocratic.conf

mv keycloak/realm.json keycloak/realm.json.old
mv keycloak/clients.json keycloak/clients.json.old
mv keycloak/client-scopes.json keycloak/client-scopes.json.old

#/opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null
#/opt/jboss/keycloak/bin/kcadm.sh get realms/master

echo "Dump realm.json"
docker exec -i isard-sso-keycloak sh -c '/opt/jboss/keycloak/bin/kcadm.sh \
    config credentials --server http://localhost:8080/auth \
    --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null && \
    /opt/jboss/keycloak/bin/kcadm.sh \
    get realms/master' > keycloak/realm.json
    
echo "Dump client-scopes.json"
docker exec -i isard-sso-keycloak sh -c '/opt/jboss/keycloak/bin/kcadm.sh \
    config credentials --server http://localhost:8080/auth \
    --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null && \
    /opt/jboss/keycloak/bin/kcadm.sh \
    get client-scopes' > keycloak/client-scopes.json

echo "Dump clients.json"
docker exec -i isard-sso-keycloak sh -c '/opt/jboss/keycloak/bin/kcadm.sh \
    config credentials --server http://localhost:8080/auth \
    --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null && \
    /opt/jboss/keycloak/bin/kcadm.sh \
    get clients' > keycloak/clients.json

kcadm.sh create realms -f - << EOF
{ "realm": "demorealm", "enabled": true }
EOF

echo ""
echo "## diff realm.json \n"
diff keycloak/realm.json keycloak/realm.json.old

echo ""
echo "## diff clients.json \n"
diff keycloak/clients.json keycloak/clients.json.old

echo ""
echo "## diff client-scopes.json \n"
diff keycloak/client-scopes.json keycloak/client-scopes.json.old



### NEW

#./kcadm.sh update realms/master -f realm.json
